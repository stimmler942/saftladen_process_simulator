package de.hamburg.itech.models;

import java.util.Random;

public class MessageTemp {
    private int temperatur;


    public MessageTemp() {
        this.temperatur = new Random().nextInt(100);
    }

    public int getTemperatur() {
        return temperatur;
    }

    public void setTemperatur(int temperatur) {
        this.temperatur = temperatur;
    }

    @Override
    public String toString() {
        return "MessageTemp{" +
                "temperatur=" + temperatur +
                '}';
    }
}

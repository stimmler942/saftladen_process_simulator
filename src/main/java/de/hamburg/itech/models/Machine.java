package de.hamburg.itech.models;

public class Machine {
    private int type;
    private int id;


    public Machine(int type, int id) {
        this.type = type;
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}

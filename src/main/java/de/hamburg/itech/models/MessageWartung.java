package de.hamburg.itech.models;

public class MessageWartung {
    private String message;
    private int state;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "MessageWartung{" +
                "message='" + message + '\'' +
                ", state=" + state +
                '}';
    }
}

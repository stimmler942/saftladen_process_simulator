package de.hamburg.itech.models;

public class MessageCount {
    private String message;
    private int count;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "MessageCount{" +
                "message='" + message + '\'' +
                ", count=" + count +
                '}';
    }
}

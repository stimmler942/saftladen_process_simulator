package de.hamburg.itech.models;

public class Pumpe {
    private int id;
    private int state;
    private String orderId;

    public Pumpe(int id, int state, String orderId) {
        this.id = id;
        this.state = state;
        this.orderId = orderId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}

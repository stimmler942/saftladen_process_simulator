package de.hamburg.itech.clients;

import com.google.gson.Gson;
import de.hamburg.itech.events.JuiceOrder;
import de.hamburg.itech.events.MachineLog;
import de.hamburg.itech.events.MachineState;
import de.hamburg.itech.models.MessageCount;
import de.hamburg.itech.models.MessagePumpe;
import de.hamburg.itech.models.MessageTemp;
import de.hamburg.itech.models.MessageWartung;
import org.eclipse.paho.client.mqttv3.*;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyMqttClient implements MqttCallback, Runnable, IMyMqttClient {
    private Gson gson = new Gson();
    private MqttClient mqttClient;
    private Thread osaftMaschine = null;
    private Thread asaftMaschine = null;
    private MachineState oSaftState = new MachineState(0, 0);
    private MachineState aSaftState = new MachineState(0, 0);

    public MyMqttClient() throws IOException {
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.submit(this);
        try {
            this.mqttClient = new MqttClient("tcp://localhost", MqttClient.generateClientId()); //10.14.58.68
            this.mqttClient.connect();
            this.mqttClient.subscribe("osaft/error");
            this.mqttClient.subscribe("osaft/log");
            this.mqttClient.subscribe("osaft/state");
            this.mqttClient.subscribe("osaft/monitoring");
            this.mqttClient.subscribe("asaft/error");
            this.mqttClient.subscribe("asaft/log");
            this.mqttClient.subscribe("asaft/state");
            this.mqttClient.subscribe("asaft/monitoring");
            this.mqttClient.subscribe("juice/order");
            this.mqttClient.setCallback(this);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void connectionLost(Throwable cause) {
        try {
            throw cause;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public void messageArrived(String topic, MqttMessage message) throws Exception {

        if (topic.contains("juice/order")) {
            JuiceOrder juiceOrder = gson.fromJson(new String(message.getPayload()), JuiceOrder.class);

            if (juiceOrder.getType() == 0) {
                if (osaftMaschine == null) {
                    oSaftState = new MachineState(0, 2);
                    osaftMaschine = new Thread(new MachineProcess(this, juiceOrder.getOrderId(), EMachine.OSAFT));
                    osaftMaschine.start();
                }
            } else {
                if (asaftMaschine == null) {
                    aSaftState = new MachineState(0, 2);
                    asaftMaschine = new Thread(new MachineProcess(this, juiceOrder.getOrderId(), EMachine.ASAFT));
                    asaftMaschine.start();
                }
            }
        }

        if (topic.contains("log")) {
            MachineLog log = gson.fromJson(new String(message.getPayload()), MachineLog.class);

            switch (log.getType()) {
                case 0: {
                    MessagePumpe pumpe = gson.fromJson(log.getMessage().toString(), MessagePumpe.class);
                    System.out.println(pumpe.toString());
                    break;
                }
                case 1: {
                    break;
                }
                case 2: {
                    MessageTemp temp = gson.fromJson(log.getMessage().toString(), MessageTemp.class);
                    System.out.println(temp.toString());
                    break;
                }
                case 3: {
                    break;
                }
                case 4: {
                    MessageWartung wartung = gson.fromJson(log.getMessage().toString(), MessageWartung.class);
                    System.out.println(wartung.toString());
                    break;
                }
                case 5: {
                    MessageCount count = gson.fromJson(log.getMessage().toString(), MessageCount.class);
                    System.out.println(count.toString());
                    break;
                }
            }
        }

        if (topic.contains("state")) {
            MachineState state = gson.fromJson(new String(message.getPayload()), MachineState.class);

            System.out.println(topic + " - " + state.toString());
        }

    }

    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(20000);

                this.mqttClient.publish("osaft/log", gson.toJson(new MachineLog(2, new MessageTemp())).getBytes(), 2, false);
                this.mqttClient.publish("osaft/state", gson.toJson(this.oSaftState).getBytes(), 2, false);
                this.mqttClient.publish("asaft/state", gson.toJson(this.aSaftState).getBytes(), 2, false);

                this.mqttClient.publish("juice/order", gson.toJson(new JuiceOrder(0, "st-123", 10)).getBytes(), 2, false);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (MqttPersistenceException e) {
                e.printStackTrace();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendMessage(String topic, Object message) {
        try {
            this.mqttClient.publish(topic, gson.toJson(message).getBytes(), 1, false);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cleanMachineProcess(EMachine machine) {
        if (EMachine.OSAFT.equals(machine)) {
            oSaftState = new MachineState(0, 0);
            osaftMaschine = null;
        } else {
            aSaftState = new MachineState(0, 0);
            asaftMaschine = null;
        }
    }

    @Override
    public void setState(EMachine machine, MachineState state) {
        if (EMachine.OSAFT.equals(machine)) {
            this.oSaftState = state;
        } else {
            this.aSaftState = state;
        }
    }
}

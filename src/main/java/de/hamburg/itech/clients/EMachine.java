package de.hamburg.itech.clients;

public enum EMachine {
    OSAFT() {
        @Override
        String getName() {
            return "osaft";
        }
    },
    ASAFT() {
        @Override
        String getName() {
            return "asaft";
        }
    };

    abstract String getName();
}

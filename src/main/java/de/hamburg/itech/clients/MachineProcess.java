package de.hamburg.itech.clients;

import de.hamburg.itech.events.MachineLog;
import de.hamburg.itech.events.MachineState;
import de.hamburg.itech.models.MessagePumpe;

public class MachineProcess implements Runnable {
    private int currentState = 0;
    private IMyMqttClient myMqttClient;
    private String orderId;
    private EMachine machine;

    public MachineProcess(IMyMqttClient myMqttClient, String orderId, EMachine machine) {
        this.myMqttClient = myMqttClient;
        this.orderId = orderId;
        this.machine = machine;
    }

    @Override
    public void run() {
        while (currentState < 5) {
            if (currentState != 0) {
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            switch (currentState) {
                case 0: {
                    this.myMqttClient.sendMessage(machine.getName() + "/state", new MachineState(0, 2));
                    this.myMqttClient.setState(machine, new MachineState(0, 2));
                    this.myMqttClient.sendMessage(machine.getName() + "/log", new MachineLog(0, new MessagePumpe(1, 1, orderId)));
                    this.currentState++;
                    break;
                }
                case 1: {
                    this.myMqttClient.sendMessage(machine.getName() + "/log", new MachineLog(0, new MessagePumpe(1, 0, orderId)));
                    this.currentState++;
                    this.myMqttClient.sendMessage(machine.getName() + "/log", new MachineLog(0, new MessagePumpe(2, 1, orderId)));
                    break;
                }
                case 2: {
                    this.myMqttClient.sendMessage(machine.getName() + "/log", new MachineLog(0, new MessagePumpe(2, 0, orderId)));
                    this.currentState++;
                    this.myMqttClient.sendMessage(machine.getName() + "/log", new MachineLog(0, new MessagePumpe(4, 1, orderId)));
                    break;
                }
                case 3: {
                    this.myMqttClient.sendMessage(machine.getName() + "/log", new MachineLog(0, new MessagePumpe(4, 0, orderId)));
                    this.currentState++;
                    this.myMqttClient.sendMessage(machine.getName() + "/log", new MachineLog(0, new MessagePumpe(3, 1, orderId)));
                    break;
                }
                case 4: {
                    this.myMqttClient.sendMessage(machine.getName() + "/log", new MachineLog(0, new MessagePumpe(3, 0, orderId)));
                    this.myMqttClient.sendMessage(machine.getName() + "/state", new MachineState(0, 0));
                    this.myMqttClient.setState(machine, new MachineState(0, 0));
                    this.currentState++;
                    break;
                }
            }
        }
        myMqttClient.cleanMachineProcess(this.machine);
    }
}
package de.hamburg.itech.clients;

import de.hamburg.itech.events.MachineState;

public interface IMyMqttClient {
    public void sendMessage(String topic, Object message);
    public void cleanMachineProcess(EMachine machine);
    public void setState(EMachine machine, MachineState state);
}

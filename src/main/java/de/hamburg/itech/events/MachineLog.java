package de.hamburg.itech.events;

import de.hamburg.itech.models.MessageCount;
import de.hamburg.itech.models.MessagePumpe;
import de.hamburg.itech.models.MessageTemp;
import de.hamburg.itech.models.MessageWartung;

public class MachineLog {
    private int type;
    private Object message;

    public MachineLog(int type, Object message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }
}

package de.hamburg.itech.events;

import de.hamburg.itech.models.Machine;

public class MachineError {

    private String message;
    private Machine machine;

    public static MachineError generateNewError(int type) {
        MachineError error = new MachineError();

        if(type == 0) {
            error.setMessage("Pumpe is defekt!");
        } else if(type == 1) {
            error.setMessage("Nivaueschalter is defekt!");
        }
        error.setMachine(new Machine(type, 123));

        return error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Machine getMachine() {
        return machine;
    }

    public void setMachine(Machine machine) {
        this.machine = machine;
    }
}

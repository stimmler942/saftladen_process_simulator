package de.hamburg.itech.events;

public class JuiceOrder {
    private int type;
    private String orderId;
    private int amount;


    public JuiceOrder(int type, String orderId, int amount) {
        this.type = type;
        this.orderId = orderId;
        this.amount = amount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}

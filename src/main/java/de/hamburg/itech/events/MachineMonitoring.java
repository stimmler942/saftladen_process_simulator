package de.hamburg.itech.events;

import de.hamburg.itech.models.Machine;
import de.hamburg.itech.models.MessagePumpe;

public class MachineMonitoring {
    private Machine machine;
    private String message;

    public MachineMonitoring(Machine machine, String message) {
        this.machine = machine;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Machine getMachine() {
        return machine;
    }

    public void setMachine(Machine machine) {
        this.machine = machine;
    }
}

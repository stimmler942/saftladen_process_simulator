package de.hamburg.itech.events;

import java.util.Random;

public class MachineState {
    private int stateType;
    private int state;

    public MachineState(int stateType, int state) {
        this.state = state;
        this.stateType = stateType;
    }

    public int getStateType() {
        return stateType;
    }

    public void setStateType(int stateType) {
        this.stateType = stateType;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "MachineState{" +
                "stateType=" + stateType +
                ", state=" + state +
                '}';
    }
}

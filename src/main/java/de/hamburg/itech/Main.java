package de.hamburg.itech;

import de.hamburg.itech.clients.MyMqttClient;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        new MyMqttClient();
    }
}
